const sidebars = {
  guide: [
    "guide/intro",
    {
      type: "category",
      label: "Getting Started",
      items: [
        "guide/getting-started/install",
      
      ],
    },
    "Keybinding"
    
  ],
  holyc : [
    "holyc/intro"
  ]


}

module.exports = sidebars
